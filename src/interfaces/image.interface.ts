export interface Image {
  title: string;
  dir: string;
  url: string;
}
