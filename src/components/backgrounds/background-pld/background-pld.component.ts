import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background-pld',
  templateUrl: './background-pld.component.html',
  styleUrls: ['./background-pld.component.scss']
})
export class BackgroundPldComponent implements OnInit {
  fullImagePath: string;

  constructor() {
    this.fullImagePath = '../../assets/img/yunius-sf.png';
  }

  ngOnInit() {
  }

}
