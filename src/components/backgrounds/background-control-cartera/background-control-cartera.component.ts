import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background-control-cartera',
  templateUrl: './background-control-cartera.component.html',
  styleUrls: ['./background-control-cartera.component.scss']
})
export class BackgroundControlCarteraComponent implements OnInit {
  fullImagePath: string;

  constructor() {
    this.fullImagePath = '../../../assets/img/yunius-sf.png';
  }

  ngOnInit() {
  }
}
