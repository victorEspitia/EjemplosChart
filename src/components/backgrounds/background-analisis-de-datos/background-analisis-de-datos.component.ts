import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background-analisis-de-datos',
  templateUrl: './background-analisis-de-datos.component.html',
  styleUrls: ['./background-analisis-de-datos.component.scss']
})
export class BackgroundAnalisisDeDatosComponent implements OnInit {
  fullImagePath: string;

  constructor() {
    this.fullImagePath = '../../../assets/img/yunius-sf.png';
  }

  ngOnInit() {
  }

}
