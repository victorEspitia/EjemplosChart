export * from './background-aclaraciones/background-aclaraciones.component';
export * from './background-ahorros/background-ahorros.component';
export * from './background-control-cartera/background-control-cartera.component';
export * from './background-monitoreo/background-monitoreo.component';
export * from './background-pld/background-pld.component';
export * from './background-autofinanciamiento/background-autofinanciamiento.component';
export * from './background-analisis-de-datos/background-analisis-de-datos.component';
