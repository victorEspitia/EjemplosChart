import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background-monitoreo',
  templateUrl: './background-monitoreo.component.html',
  styleUrls: ['./background-monitoreo.component.scss']
})
export class BackgroundMonitoreoComponent implements OnInit {
  fullImagePath: string;

  constructor() {
    this.fullImagePath = '../../../assets/img/yunius-sf.png';
  }

  ngOnInit() {
  }

}
