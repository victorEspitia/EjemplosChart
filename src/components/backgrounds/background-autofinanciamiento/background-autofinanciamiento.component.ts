import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background-autofinanciamiento',
  templateUrl: './background-autofinanciamiento.component.html',
  styleUrls: ['./background-autofinanciamiento.component.scss']
})
export class BackgroundAutofinanciamientoComponent implements OnInit {
  fullImagePath: string;

  constructor() {
    this.fullImagePath = '../../../assets/img/yunius-sf.png';
  }

  ngOnInit() {
  }

}
