import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background-aclaraciones',
  templateUrl: './background-aclaraciones.component.html',
  styleUrls: ['./background-aclaraciones.component.scss']
})
export class BackgroundAclaracionesComponent implements OnInit {
  fullImagePath: string;

  constructor() {
    this.fullImagePath = '../../../assets/img/yunius-sf.png';
  }

  ngOnInit() {
  }

}
