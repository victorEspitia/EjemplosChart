import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background-ahorros',
  templateUrl: './background-ahorros.component.html',
  styleUrls: ['./background-ahorros.component.scss']
})
export class BackgroundAhorrosComponent implements OnInit {
  fullImagePath: string;

  constructor() {
    this.fullImagePath = '../../../assets/img/yunius-sf.png';
  }

  ngOnInit() {
  }

}
