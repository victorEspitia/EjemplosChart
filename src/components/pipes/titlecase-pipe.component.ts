import { Component, Input } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';

@Component({
  selector: 'app-titlecase-pipe',
  template: `{{ header.title }}`,
  styles: []
})

export class TitleCasePipeComponent {

  constructor(private header: HeaderService) {
    this.header = header;
   }

}
