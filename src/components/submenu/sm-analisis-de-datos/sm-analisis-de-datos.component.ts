import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sm-analisis-de-datos',
  templateUrl: './sm-analisis-de-datos.component.html',
  styleUrls: ['./sm-analisis-de-datos.component.css']
})
export class SmAnalisisDeDatosComponent implements OnInit {
  catalogos = [
    {text: 'Importación de datos', icon: '', url: '/analisis-de-datos/catalogos/importacion'}
  ];

  reportes = [
    {text: 'Reporte RIP', icon: '', url: '/analisis-de-datos/reportes/reporte-rip'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
