import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmAnalisisDeDatosComponent } from './sm-analisis-de-datos.component';

describe('SmAnalisisDeDatosComponent', () => {
  let component: SmAnalisisDeDatosComponent;
  let fixture: ComponentFixture<SmAnalisisDeDatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmAnalisisDeDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmAnalisisDeDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
