import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmMonitoreoComponent } from './sm-monitoreo.component';

describe('SmMonitoreoComponent', () => {
  let component: SmMonitoreoComponent;
  let fixture: ComponentFixture<SmMonitoreoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmMonitoreoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmMonitoreoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
