import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sm-monitoreo',
  templateUrl: './sm-monitoreo.component.html',
  styleUrls: ['./sm-monitoreo.component.css']
})
export class SmMonitoreoComponent implements OnInit {
  catalogos = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  operacion = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  reportes = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
