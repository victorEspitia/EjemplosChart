import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sm-pld',
  templateUrl: './sm-pld.component.html',
  styleUrls: ['./sm-pld.component.css']
})
export class SmPLDComponent implements OnInit {
  catalogos = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  operacion = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  reportes = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
