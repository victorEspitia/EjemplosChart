import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmPLDComponent } from './sm-pld.component';

describe('SmPLDComponent', () => {
  let component: SmPLDComponent;
  let fixture: ComponentFixture<SmPLDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmPLDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmPLDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
