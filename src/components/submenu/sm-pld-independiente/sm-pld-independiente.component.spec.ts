import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmPLDIndependienteComponent } from './sm-pld-independiente.component';

describe('SmPLDComponent', () => {
  let component: SmPLDIndependienteComponent;
  let fixture: ComponentFixture<SmPLDIndependienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmPLDIndependienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmPLDIndependienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
