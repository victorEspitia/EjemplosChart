import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmControlDeCarteraComponent } from './sm-control-cartera.component';

describe('SmControlDeCarteraComponent', () => {
  let component: SmControlDeCarteraComponent;
  let fixture: ComponentFixture<SmControlDeCarteraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmControlDeCarteraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmControlDeCarteraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
