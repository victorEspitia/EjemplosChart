import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sm-control-cartera',
  templateUrl: './sm-control-cartera.component.html',
  styleUrls: ['./sm-control-cartera.component.css']
})
export class SmControlDeCarteraComponent implements OnInit {
  catalogos = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  operacion = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  reportes = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
