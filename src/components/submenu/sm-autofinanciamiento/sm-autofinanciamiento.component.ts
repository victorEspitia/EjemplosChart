import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sm-autofinanciamiento',
  templateUrl: './sm-autofinanciamiento.component.html',
  styleUrls: ['./sm-autofinanciamiento.component.css']
})
export class SmAutofinanciamientoComponent implements OnInit {
  catalogos = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  operacion = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  reportes = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
