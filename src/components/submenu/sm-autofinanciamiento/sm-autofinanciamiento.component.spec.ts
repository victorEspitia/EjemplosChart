import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmAutofinanciamientoComponent } from './sm-autofinanciamiento.component';

describe('SmControlDeCarteraComponent', () => {
  let component: SmAutofinanciamientoComponent;
  let fixture: ComponentFixture<SmAutofinanciamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmAutofinanciamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmAutofinanciamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
