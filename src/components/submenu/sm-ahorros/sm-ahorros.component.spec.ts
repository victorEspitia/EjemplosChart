import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmAhorrosComponent } from './sm-ahorros.component';

describe('SmAhorrosComponent', () => {
  let component: SmAhorrosComponent;
  let fixture: ComponentFixture<SmAhorrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmAhorrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmAhorrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
