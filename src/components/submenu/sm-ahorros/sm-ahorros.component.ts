import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sm-ahorros',
  templateUrl: './sm-ahorros.component.html',
  styleUrls: ['./sm-ahorros.component.css']
})
export class SmAhorrosComponent implements OnInit {
  catalogos = [
    {text: 'Esquema de tasa de intrés', icon: '', url: '/ahorros/catalogos/esquema'},
    {text: 'Producto de ahorro', icon: '', url: '/ahorros/catalogos/producto'}
  ];

  operacion = [
    {text: 'Solicitud de cuentas de ahorro', icon: '', url: '/ahorros/operacion/solicitud'}
  ];

  reportes = [
    {text: 'Reporte IDE', icon: '', url: '/ahorros/reportes/reporteide'},
    {text: 'Cuentas de ahorro por esquema', icon: '', url: '/ahorros/reportes/cuentasahorro'},
    {text: 'Estado de cuenta', icon: '', url: '/ahorros/reportes/estadodecuenta'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
