import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sm-aclaraciones',
  templateUrl: './sm-aclaraciones.component.html',
  styleUrls: ['./sm-aclaraciones.component.css']
})
export class SmAclaracionesComponent implements OnInit {
  catalogos = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  operacion = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  reportes = [
    {text: 'OPC_1', icon: '', url: '#'},
    {text: 'OPC_2', icon: '', url: '#'},
    {text: 'OPC_3', icon: '', url: '#'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
