import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmAclaracionesComponent } from './sm-aclaraciones.component';

describe('SmAclaracionesComponent', () => {
  let component: SmAclaracionesComponent;
  let fixture: ComponentFixture<SmAclaracionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmAclaracionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmAclaracionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
