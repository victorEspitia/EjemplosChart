import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { HeaderService, ToolBarMode } from './header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  @Input() submenu: string;
  @Input() visible: string;
  @Input() active: boolean;

  @Output() PasameElParametro = new EventEmitter();
  public mensaje: string;

  title = '';
  isAuthenticated = false;
  angularImage: string;

  catalogosA = [
    {text: 'Esquema de tasa de intrés', icon: '', url: '/ahorros/catalogos/esquema'},
    {text: 'Producto de ahorro', icon: '', url: '/ahorros/catalogos/productos'}
  ];
  operacionA = [
    {text: 'Solicitud de cuentas de ahorro', icon: '', url: '/ahorros/operacion/solicitud'}
  ];
  reportesA = [
    {text: 'Reporte IDE', icon: '', url: '/ahorros/reportes/reporteide'},
    {text: 'Cuentas de ahorro por esquema', icon: '', url: '/ahorros/reportes/cuentasahorro'},
    {text: 'Estado de cuenta', icon: '', url: '/ahorros/reportes/estadodecuenta'}
  ];

  // tslint:disable-next-line:member-ordering
  static parameters = [Router, HeaderService];

  // tslint:disable-next-line:no-shadowed-variable
  constructor(private Router: Router, private HeaderService: HeaderService, private location: Location) {
    this.angularImage = '/assets/img/logo_enterprice.png';
    this.Router = Router;
    this.HeaderService = HeaderService;
  }

  ngOnInit() {
    console.log(this.visible);
    console.log(this.submenu);
  }

  public showMenu(login: boolean): boolean {
    console.log('muestraHeader', login);
    if (login) {
      return false;
    } else {
      return true;
    }
  }

  hide () {
    this.active = false;
  }

  showToolbar(): boolean {
    if (this.HeaderService.mode === ToolBarMode.None) {
      return false;
    } else {
      return true;
    }
  }

  getMode(): string {
    if (this.HeaderService.mode === ToolBarMode.Menu) {
      return 'menu';
    } else if (this.HeaderService.mode === ToolBarMode.Return) {
      return 'return';
    } else {
      return 'default';
    }
  }

  logout(): void {
    this.Router.navigateByUrl('/');
  }

  openMenu(): void {
    console.log('Abierto');
  }

  goBack(): void {
    this.location.back();
  }

  reset() {
    console.log('reset');
  }
}
