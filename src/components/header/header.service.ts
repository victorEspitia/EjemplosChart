'use strict';
import { Injectable } from '@angular/core';

@Injectable()
export class HeaderService {
  public mode: ToolBarMode;
  public title: string;
  public modeRow: ToolBarRowMode;

  constructor() {
    this.mode = ToolBarMode.Menu;
    this.title = 'Yunius';
  }

  showToolbar(): boolean {
    if (ToolBarMode.None) {
      return false;
    } else {
      return true;
    }
  }

}

export enum ToolBarMode {
  None,
  Menu,
  Return
}

export enum ToolBarRowMode {
  None,
  Aclaraciones,
  Ahorros,
  Analisis,
  Autofinanciamiento,
  ControlCartera,
  Monitoreo,
  Pld,
  PldIndependiente
}
