import { Component } from '@angular/core';

@Component({
  selector: 'app-in-maintenance',
  templateUrl: './in-maintenance.component.html',
  styleUrls: ['./in-maintenance.component.scss']
})

export class PageInMaintenanceComponent {
  fullImagePath: string;

  constructor() {
    this.fullImagePath = '../../../assets/img/maintenance.jpg';
  }

}
