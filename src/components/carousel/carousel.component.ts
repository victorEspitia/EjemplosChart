import { Component } from '@angular/core';
import { Image } from '../../interfaces';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})

export class CarouselComponent {
  public images = IMAGES;
  constructor() {
  }
}

const IMAGES: Image[] = [
  { 'title': 'Yunius', 'dir': '/assets/img/yunius01.png', 'url': 'http://www.yunius.com' },
  { 'title': 'Buddless', 'dir': '/assets/img/buddless01.png', 'url': 'http://www.buddless.com' },
  { 'title': 'Microfinance intelligence', 'dir': '/assets/img/yunius02.png', 'url': 'http://www.yunius.com' },
  { 'title': 'Prevención de lavado de dinero', 'dir': '/assets/img/buddless02.png', 'url': 'http://www.yunius.com' },
  { 'title': 'DAComp SC', 'dir': '/assets/img/dacomp.png', 'url': 'http://www.dacompsc.com' },
];
