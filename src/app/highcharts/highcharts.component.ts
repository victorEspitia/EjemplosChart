import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';

@Component({
  selector: 'app-highcharts',
  templateUrl: './highcharts.component.html',
  styleUrls: ['./highcharts.component.css']
})
export class HighchartsComponent implements OnInit {

  constructor(private header: HeaderService) {
    this.header.title = 'HighCharts';
    this.header.modeRow = 5;
  }

  options: Object;
  
  ngOnInit() {
    this.options = {
      title : { text : 'simple chart' },
      series: [{
          data: [29.9, 71.5, 106.4, 129.2],
      }]
  };
  }

}
