import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';

@Component({
  selector: 'app-fusioncharts',
  templateUrl: './fusioncharts.component.html',
  styleUrls: ['./fusioncharts.component.css']
})
export class FusionchartsComponent implements OnInit {

  constructor(private header: HeaderService) {
    this.header.title = 'Fusion Charts';
    this.header.modeRow = 3;
  }

  data: Object;
  
  ngOnInit() {
    this.data = {
      chart: { },
      data: [
        {value: 500},
        {value: 600},
        {value: 700}
      ]
    };
  }
}
