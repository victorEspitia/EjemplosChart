import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
//import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import { Subject } from 'rxjs';

@Injectable()
export class LoggedInGuard implements CanActivate {
    constructor() {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return new Subject;
    }
}
