import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

// 404 page

import { PageNotFoundComponent } from '../components/not-found/not-found.component';

// Pages

// Components

import { AngularNvd3Component } from './angular-nvd3/angular-nvd3.component';
import { ChartJsComponent } from './chart-js/chart-js.component';
import { FusionchartsComponent } from './fusioncharts/fusioncharts.component';
import { GoogleChartComponent } from './google-chart/google-chart.component';
import { HighchartsComponent } from './highcharts/highcharts.component';
import { JqwidgetsChartComponent } from './jqwidgets-chart/jqwidgets-chart.component';
import { KendoAngularUiComponent } from './kendo-angular-ui/kendo-angular-ui.component';
import { Ng2ChartsComponent } from './ng2-charts/ng2-charts.component';
import { NgxChartsComponent } from './ngx-charts/ngx-charts.component';
import { PrimengUiComponent } from './primeng-ui/primeng-ui.component';

// Protected


// Routing
const appRoutes: Routes = [
  // Public pages
  { path: '', redirectTo: '/chart-nvd3', pathMatch : 'full' },
  { path: 'chart-nvd3', component: AngularNvd3Component },
  { path: 'chart-js', component: ChartJsComponent },
  { path: 'chart-fusion', component: FusionchartsComponent  },
  { path: 'chart-google', component: GoogleChartComponent },
  { path: 'chart-high', component: HighchartsComponent },
  { path: 'chart-jqwidgets', component: JqwidgetsChartComponent },
  { path: 'chart-kendo', component: KendoAngularUiComponent },
  { path: 'chart-ng2', component: Ng2ChartsComponent },
  { path: 'chart-ngx', component: NgxChartsComponent },
  { path: 'chart-primeng', component: PrimengUiComponent },
  
  // Pague not found
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }