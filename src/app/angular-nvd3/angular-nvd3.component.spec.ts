import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularNvd3Component } from './angular-nvd3.component';

describe('AngularNvd3Component', () => {
  let component: AngularNvd3Component;
  let fixture: ComponentFixture<AngularNvd3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularNvd3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularNvd3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
