import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';

@Component({
  selector: 'app-primeng-ui',
  templateUrl: './primeng-ui.component.html',
  styleUrls: ['./primeng-ui.component.css']
})
export class PrimengUiComponent implements OnInit {

  constructor(private header: HeaderService) {
    this.header.title = 'PrimeNG UI';
    this.header.modeRow = 10;
  }

  data: any;
  
  ngOnInit() {
    this.data = {
      labels: ['A','B','C'],
      datasets: [
          {
              data: [300, 50, 100],
              backgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
              ],
              hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
              ]
          }]    
      };
  }

}
