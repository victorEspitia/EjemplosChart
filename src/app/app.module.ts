import { NgModule,
  ErrorHandler,
  Injectable,
  ApplicationRef,
  Provider,
  OnInit,
  OnDestroy } from '@angular/core';
import { Http,
  HttpModule,
  BaseRequestOptions,
  RequestOptions,
  RequestOptionsArgs } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialog,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatIcon,
  MatIconRegistry,
  MatHorizontalStepper,
  MatDividerModule } from '@angular/material';
import { FormsModule,
  ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CdkTableModule } from '@angular/cdk/table';

// Modulos preinstalados
import {FlexLayoutModule} from '@angular/flex-layout';
import { CookieService } from 'ngx-cookie-service';

import { RouterModule,
  Routes } from '@angular/router';

// Pipes
import { PipesModule } from '../components/pipes/pipes.module';

import { AppRoutingModule } from './app.routing';

import { AngularNvd3Component } from './angular-nvd3/angular-nvd3.component';
import { Ng2ChartsComponent } from './ng2-charts/ng2-charts.component';
import { PrimengUiComponent } from './primeng-ui/primeng-ui.component';
import { FusionchartsComponent } from './fusioncharts/fusioncharts.component';
import { NgxChartsComponent } from './ngx-charts/ngx-charts.component';
import { GoogleChartComponent } from './google-chart/google-chart.component';
import { HighchartsComponent } from './highcharts/highcharts.component';
import { KendoAngularUiComponent } from './kendo-angular-ui/kendo-angular-ui.component';
import { JqwidgetsChartComponent } from './jqwidgets-chart/jqwidgets-chart.component';
import { ChartJsComponent } from './chart-js/chart-js.component';
import { PageNotFoundComponent } from '../components/not-found/not-found.component';
import { FooterComponent } from '../components/footer/footer.component';
import { HeaderComponent } from '../components/header/header.component';
import { HeaderService } from '../components/header/header.service';
import { CarouselComponent } from '../components/carousel/carousel.component';
import { TableComponent } from '../components/table/table.component';
import { Interceptor } from './object/universal/interceptor';

// NVD3 Chart
import { NvD3Module } from 'ng2-nvd3';
import 'd3';
import 'nvd3';

//Ng2 Chart
import { ChartsModule } from 'ng2-charts/ng2-charts';

//Primeng Chart
import  {ChartModule as ChartPrimeng} from 'primeng/chart';
//FusionCharts
import { FusionChartsModule } from 'angular2-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';

//NgX Chart
import { NgxChartsModule } from '@swimlane/ngx-charts';
 
//Google Chart
import {GoogleChart} from 'angular2-google-chart/directives/angular2-google-chart.directive';

//HighCharts
import { ChartModule as ChartHigh } from 'angular2-highcharts';

//Kendo
import { ChartsModule as kendoChart } from '@progress/kendo-angular-charts';
import { SparklineModule } from '@progress/kendo-angular-charts';
import 'hammerjs';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { LabelModule } from '@progress/kendo-angular-label';

// Main
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    FooterComponent,
    HeaderComponent,
    CarouselComponent,
    TableComponent,
    AngularNvd3Component,
    Ng2ChartsComponent,
    PrimengUiComponent,
    FusionchartsComponent,
    NgxChartsComponent,
    GoogleChartComponent,
    HighchartsComponent,
    KendoAngularUiComponent,
    JqwidgetsChartComponent,
    ChartJsComponent
  ],
  imports: [
   BrowserModule,
   NvD3Module,
   ChartsModule,
   ChartPrimeng,
   FusionChartsModule.forRoot(FusionCharts, Charts),
   NgxChartsModule,
   kendoChart,
   SparklineModule,
   ButtonsModule,
   LabelModule,
   ChartHigh.forRoot(require('highcharts')),
    BrowserAnimationsModule,
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    PipesModule,
    FlexLayoutModule,
    MatDividerModule,
  ],
  providers: [
    HeaderService,
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true },
    CookieService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
