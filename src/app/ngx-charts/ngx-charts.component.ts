import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { single, multi } from './data';

@Component({
  selector: 'app-ngx-charts',
  templateUrl: './ngx-charts.component.html',
  styleUrls: ['./ngx-charts.component.css']
})
export class NgxChartsComponent implements OnInit {

  constructor(private header: HeaderService) {
    this.header.title = 'NGx Charts';
    this.header.modeRow = 9;
    Object.assign(this, { single, multi })
  }

  single: any[];
  multi: any[];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  ngOnInit() {
  }

}
