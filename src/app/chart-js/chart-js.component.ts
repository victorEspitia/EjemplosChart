import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';

@Component({
  selector: 'app-chart-js',
  templateUrl: './chart-js.component.html',
  styleUrls: ['./chart-js.component.css']
})
export class ChartJsComponent implements OnInit {

  constructor(private header: HeaderService) {
    this.header.title = 'Angular.js';
    this.header.modeRow = 2;
  }

  ngOnInit() {
  }

}
