import { Component, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MediaMatcher } from '@angular/cdk/layout';
import { HeaderService, ToolBarMode, ToolBarRowMode } from '../components/header/header.service';
import { TitleCasePipeComponent } from '../components/pipes/titlecase-pipe.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  mobileQuery: MediaQueryList;
  fillerNav = Array(50).fill(0).map((_, i) => `Nav Item ${i + 1}`);
  mainMenu = [
    {text: 'Chart NVD3', icon: 'label', url: '/chart-nvd3'},
    {text: 'Chart Chart.js', icon: 'label', url: '/chart-js'},
    {text: 'Chart Fusion', icon: 'label', url: '/chart-fusion'},
    {text: 'Chart Google', icon: 'label', url: '/chart-google'},
    {text: 'Chart HighCharts', icon: 'label', url: '/chart-high'},
    {text: 'Chart JQWidgets', icon: 'label', url: '/chart-jqwidgets'},
    {text: 'Chart Kendo', icon: 'label', url: '/chart-kendo'},
    {text: 'Chart Ng2', icon: 'label', url: '/chart-ng2'},
    {text: 'Chart Ngx', icon: 'label', url: '/chart-ngx'},
    {text: 'Chart Prime Ng', icon: 'label', url: '/chart-primeng'}
  ];
  private _mobileQueryListener: () => void;

  constructor(private http: HttpClient, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private header: HeaderService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.header = header;

    /*http.get('/api/test').subscribe(
      (result: any) => {
        this.respuesta = result.estado;
      },
      (err) => {
        console.log(err);
        this.respuesta = 'Hubo un error con la petición: ' + err;
      });*/
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  showToolbar(): boolean {
    if (this.header.mode === ToolBarMode.None) {
      return false;
    } else {
      return true;
    }
  }

  getMode(): string {
    if (this.header.mode === ToolBarMode.Menu) {
      return 'menu';
    } else if (this.header.mode === ToolBarMode.Return) {
      return 'return';
    } else {
      return 'default';
    }
  }

  // tslint:disable-next-line:member-ordering
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));

}
