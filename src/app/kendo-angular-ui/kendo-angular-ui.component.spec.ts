import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KendoAngularUiComponent } from './kendo-angular-ui.component';

describe('KendoAngularUiComponent', () => {
  let component: KendoAngularUiComponent;
  let fixture: ComponentFixture<KendoAngularUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KendoAngularUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KendoAngularUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
