import { Component, OnInit, NgModule } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';

@Component({
  selector: 'app-kendo-angular-ui',
  templateUrl: './kendo-angular-ui.component.html',
  styleUrls: ['./kendo-angular-ui.component.scss']
})
export class KendoAngularUiComponent implements OnInit {

  constructor(private header: HeaderService) {
    this.header.title = 'Kendo Angular UI';
    this.header.modeRow = 7;
  }

  ngOnInit() {
  }

  title = 'Hello World!';

    onButtonClick() {
        this.title = 'Hello from Kendo UI!';
    }

    public weatherData = [
      { month: "January", min: 5, max: 11 },
      { month: "February", min: 5, max: 13 },
      { month: "March", min: 7, max: 15 },
      { month: "April", min: 10, max: 19 },
      { month: "May", min: 13, max: 23 },
      { month: "June", min: 17, max: 28 },
      { month: "July", min: 20, max: 30 },
      { month: "August", min: 20, max: 30 },
      { month: "September", min: 17, max: 26 },
      { month: "October", min: 13, max: 22 },
      { month: "November", min: 9, max: 16 },
      { month: "December", min: 6, max: 13 }
    ];
  
    public labelContentFrom(e: any): string {
        return `${ e.value.from } °C`;
    }
  
    public labelContentTo(e: any): string {
        return `${ e.value.to } °C`;
    }

    public cashFlowData = [{
      period: 'Beginning\\nBalance',
      amount: 50000
    }, {
      period: 'Jan',
      amount: 17000
    }, {
      period: 'Feb',
      amount: 14000
    }, {
      period: 'Mar',
      amount: -12000
    }, {
      period: 'Q1',
      summary: 'runningTotal'
    }, {
      period: 'Apr',
      amount: -22000
    }, {
      period: 'May',
      amount: -18000
    }, {
      period: 'Jun',
      amount: 10000
    }, {
      period: 'Q2',
      summary: 'runningTotal'
    }, {
      period: 'Ending\\nBalance',
      summary: 'total'
    }];
  
    public pointColor(point: any): string {
      var summary = point.dataItem.summary;
      if (summary) {
        return summary == 'total' ? '#555' : 'gray';
      }
  
      if (point.value > 0) {
        return 'green';
      } else {
        return 'red';
      }
    }
}
