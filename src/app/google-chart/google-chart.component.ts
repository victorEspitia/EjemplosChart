import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';

@Component({
  selector: 'app-google-chart',
  templateUrl: './google-chart.component.html',
  styleUrls: ['./google-chart.component.css']
})
export class GoogleChartComponent implements OnInit {

  constructor(private header: HeaderService) {
    this.header.title = 'Google Charts';
    this.header.modeRow = 4;
  }

  public gauge_ChartData = [
    ['Label', 'Value'],
    ['Systolic', 120],
    ['Diastolic', 80]];
public gauge_ChartOptions = {
    width: 400, height: 120,
    redFrom: 90, redTo: 100,
    yellowFrom: 75, yellowTo: 90,
    minorTicks: 5
};

  ngOnInit() {
  }

}
