import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JqwidgetsChartComponent } from './jqwidgets-chart.component';

describe('JqwidgetsChartComponent', () => {
  let component: JqwidgetsChartComponent;
  let fixture: ComponentFixture<JqwidgetsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JqwidgetsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JqwidgetsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
