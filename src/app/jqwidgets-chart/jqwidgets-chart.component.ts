import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../components/header/header.service';

@Component({
  selector: 'app-jqwidgets-chart',
  templateUrl: './jqwidgets-chart.component.html',
  styleUrls: ['./jqwidgets-chart.component.css']
})
export class JqwidgetsChartComponent implements OnInit {

  constructor(private header: HeaderService) {
    this.header.title = 'JQWidgets';
    this.header.modeRow = 6;
  }

  ngOnInit() {
  }

}
